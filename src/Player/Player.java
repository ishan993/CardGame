package Player;

import Cards.Card;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by ishanvadwala on 7/7/17.
 */
public class Player implements IPlayer {
    ArrayList<Card> playerDeck;
    private String name;
    private int score;

    public Player(String name) {
        this.name = name;
    }

    public void setDeck(ArrayList<Card> playerDeck) {
        this.playerDeck = playerDeck;
    }

    public String getName() {
        return name;
    }

    public void clearDeck() {
        playerDeck.clear();
    }

    public Card getTopCard() {
        if(playerDeck.isEmpty()){
            throw new Error("No cards in hand. Please start or restart the game");
        }

        return playerDeck.remove(0);
    }

    public int getScore() {
        System.out.println(name + " scored "+ score + " points");
        return score;
    }

    public void resetScore() {
        score = 0;
    }

    @Override
    public void incrementScore() {
        score++;
    }
}
