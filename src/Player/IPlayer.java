package Player;

import Cards.Card;

import java.util.ArrayList;

/**
 * Created by ishanvadwala on 7/7/17.
 */
public interface IPlayer {

    void setDeck(ArrayList<Card> cardDeck);

    String getName();

    void clearDeck();

    Card getTopCard();

    int getScore();

    void resetScore();

    void incrementScore();
}
