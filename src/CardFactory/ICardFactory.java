package CardFactory;

import Cards.Card;

import java.util.ArrayList;

/**
 * Created by ishanvadwala on 7/7/17.
 */
public interface ICardFactory {

    ArrayList<Card> getCardsOfThisSuite(String color);
}
