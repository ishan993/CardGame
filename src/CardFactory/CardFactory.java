package CardFactory;

import Cards.Card;
import Cards.DiamondsCard;
import Cards.HeartsCard;
import Cards.SpadesCard;

import java.util.ArrayList;

/**
 * Created by ishanvadwala on 7/7/17.
 */
public class CardFactory implements ICardFactory {
    private final int cardsPerSuite = 13;

    public CardFactory(){}

    public ArrayList<Card> getCardsOfThisSuite(String color) {
        ArrayList<Card> list = new ArrayList<>();

        for (int i = 1; i <= cardsPerSuite; i++){
            Card card = instantiateCard(color);
            card.setCardValue(i);
            list.add(card);
        }
        return list;
    }


    // Used a factory method pattern.
    // I'd read about the flyweight pattern but never implemented it.
    // And I always wondered how it would function properly since each object
    // stored in a hashmap would point to the same memory location
    public Card instantiateCard(String color) {


        switch(color){
            case "Hearts":
                return new HeartsCard();
            case "Clubs":
                return new HeartsCard();
            case "Diamonds":
                return new DiamondsCard();
            case "Spades":
                return new SpadesCard();
            default:
                return null;
        }
    }
}
