package Cards;

/**
 * Created by ishanvadwala on 7/7/17.
 */
public abstract class Card {
    int cardValue;

    public Card( ){
    }

    public void setCardValue(int value){
        this.cardValue = value;
    }

    public int getCardValue(){
        return cardValue;
    }

    public abstract int getCardPriority();

    public abstract String getSuite();
}
