package Cards;

/**
 * Created by ishanvadwala on 7/7/17.
 */
public class SpadesCard extends Card{
    private final int cardPriority = 4;
    private final String suite = "Spades";

    public int getCardPriority() {
        return cardPriority;
    }

    @Override
    public String getSuite() {
        return suite;
    }

}
