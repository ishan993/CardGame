package Cards;

/**
 * Created by ishanvadwala on 7/7/17.
 */
public class ClubsCard extends Card {
    private final String suite = "Clubs";
    private final static int cardPriority = 3;

    public int getCardPriority() {
        return cardPriority;
    }

    @Override
    public String getSuite() {
        return suite;
    }


}
