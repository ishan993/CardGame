package Cards;

/**
 * Created by ishanvadwala on 7/7/17.
 */
public class DiamondsCard extends Card {
    private final int cardPriority = 2;
    private final String suite = "Diamonds";

    public int getCardPriority() {
        return cardPriority;
    }

    @Override
    public String getSuite() {
        return suite;
    }
}
