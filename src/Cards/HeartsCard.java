package Cards;

/**
 * Created by ishanvadwala on 7/7/17.
 */
public class HeartsCard extends Card {
    private final int cardPriority = 1;
    private final String suite = "Hearts";

    public int getCardPriority() {
        return cardPriority;
    }

    @Override
    public String getSuite() {
        return suite;
    }

}
