import CardFactory.CardFactory;
import CardGame.CardGame;
import Cards.Card;
import DeckOfCards.DeckOfCards;
import Player.Player;

import java.util.ArrayList;

/**
 * Created by ishanvadwala on 7/7/17.
 */
public class GameTester {
    public static enum CardSuites{
        Hearts, Diamonds, Clubs, Spades;
    }

    public static void main(String[] args){

        System.out.println("Hello World!");

        // Get cards from the car factory
        CardFactory cardFactory = new CardFactory();

        ArrayList<Card> hearts = cardFactory.getCardsOfThisSuite(String.valueOf(CardSuites.Hearts));
        ArrayList<Card> spades = cardFactory.getCardsOfThisSuite(String.valueOf(CardSuites.Spades));
        ArrayList<Card> clubs = cardFactory.getCardsOfThisSuite(String.valueOf(CardSuites.Clubs));
        ArrayList<Card> diamonds = cardFactory.getCardsOfThisSuite(String.valueOf(CardSuites.Diamonds));

        // Add them to a deck
        DeckOfCards deck = new DeckOfCards();
        deck.addToDeck(hearts);
        deck.addToDeck(spades);
        deck.addToDeck(clubs);
        deck.addToDeck(diamonds);

        // Create players
        Player mario = new Player("Mario");
        Player luigi = new Player("Luigi");
        Player peach = new Player("Peach");

        // Initialize the card game
        CardGame cardGame = new CardGame();

        // Set the deck of cards for the game
        cardGame.setFullDeck(deck);

        // Add players to the game
        cardGame.addPlayer(mario);
        cardGame.addPlayer(luigi);
        cardGame.addPlayer(peach);

        // Shuffle all the cards before playing
        cardGame.shuffle();

        // Start the game
        cardGame.start();

        // Each player plays with the topmost card in their hand
        cardGame.playHand();
        cardGame.playHand();
        cardGame.playHand();
        cardGame.playHand();
        cardGame.playHand();
        cardGame.playHand();
        cardGame.playHand();


        cardGame.endGame();
    }
}
