package CardGame;

import Cards.Card;
import DeckOfCards.DeckOfCards;
import Player.Player;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by ishanvadwala on 7/7/17.
 */
public class CardGame implements ICardGame {
    private ArrayList<Card> fullDeck;
    private ArrayList<Player> playerList = new ArrayList<Player>();
    private DeckOfCards deck;

    public CardGame(){}

    public void addPlayer(Player player) {
        playerList.add(player);
    }

    public void setFullDeck(DeckOfCards deckOfCards) {
        this.deck = deckOfCards;
        fullDeck = deck.getDeckOfCards();
    }

    @Override
    public void clearFullDeck() {
        fullDeck.clear();
    }

    public void playHand() {
        Player winner = null;
        Card winningCard = null;
        int maxCardVal = Integer.MIN_VALUE;

        if(playerList.size() < 2){
            throw new Error("Not enough players. Please add 2 or more players to the game");
        }

        System.out.println();
        for(Player player: playerList){
            Card currentCard = player.getTopCard();

            System.out.println(player.getName()+" played card: "+currentCard.getCardValue() +
                    " of " + currentCard.getSuite());

            if(currentCard.getCardValue() > maxCardVal){
                winner = player;
                winningCard = currentCard;
                maxCardVal = winningCard.getCardValue();

            } else if (currentCard.getCardValue() == maxCardVal) {

                if(currentCard.getCardPriority() > winningCard.getCardPriority()){
                    winner = player;
                    winningCard = currentCard;
                }else if(currentCard.getCardPriority() == winningCard.getCardPriority()){

                    System.out.println("There's been a tie!");
                }
            }
        }
        System.out.println(winner.getName() + " won this hand with card: "+winningCard.getCardValue()+
                " of "+winningCard.getSuite());
        winner.incrementScore();
    }

    public void shuffle() {
        Random random = new Random();

        if(fullDeck == null){
            throw new Error("No cards to shuffle. Please add a deck of cards");
        }
        int size = fullDeck.size();
        for (int i = 0; i < size; i++){
            int randomIndex = random.nextInt(i+1);
            // Swapping Cards
            Card tempCard = fullDeck.get(randomIndex);
            fullDeck.set(randomIndex, fullDeck.get(i));
            fullDeck.set(i, tempCard);
        }

    }

    public void start() {

        int cardsPerPerson = fullDeck.size()/playerList.size();
        System.out.println("Each player gets " + cardsPerPerson + " cards");

        for (Player player: playerList) {
            ArrayList<Card> playerDeck = new ArrayList<>();
            for (int i = 0; i < cardsPerPerson; i++) {
                playerDeck.add(fullDeck.remove(0));
            }
            player.setDeck(playerDeck);
        }

        System.out.println("Starting Game");
    }

    public void endGame() {
        Player winner = null;
        int maxScore = Integer.MIN_VALUE;

        System.out.println();

        for (Player player: playerList) {
            int currScore = player.getScore();
            if(currScore > maxScore){
                maxScore = currScore;
                winner = player;
            }
            player.resetScore();
            player.clearDeck();
        }

        clearFullDeck();

        System.out.println();
        System.out.println("Thank you for playing!");
    }
}
