package CardGame;

import Cards.Card;
import DeckOfCards.DeckOfCards;
import Player.Player;

import java.util.ArrayList;

/**
 * Created by ishanvadwala on 7/7/17.
 */
public interface ICardGame {

    void addPlayer(Player player);

    void setFullDeck(DeckOfCards deckOfCards);

    void clearFullDeck();

    void playHand();

    void shuffle();

    void start();

    void endGame();
}
