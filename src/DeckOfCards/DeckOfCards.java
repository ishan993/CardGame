package DeckOfCards;

import Cards.Card;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by ishanvadwala on 7/10/17.
 */
public class DeckOfCards implements IDeckOfCards {
    ArrayList<Card> deck;

    public DeckOfCards(){
        deck = new ArrayList<>();
    }

    public void addToDeck(ArrayList<Card> cards) {
        deck.addAll(cards);
    }

    public ArrayList<Card> getDeckOfCards() {
        return deck;
    }

    @Override
    public void clearDeck() {
        deck.clear();
    }
}
