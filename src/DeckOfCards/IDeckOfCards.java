package DeckOfCards;

import Cards.Card;

import java.util.ArrayList;

/**
 * Created by ishanvadwala on 7/10/17.
 */
public interface IDeckOfCards {
    void addToDeck(ArrayList<Card> cards);

    ArrayList<Card> getDeckOfCards();

    void clearDeck();
}
