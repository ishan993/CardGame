A simple card game where a player wins based on the value and priority of their card.

Steps:
1. Use the Card factory to generate cards of a particular suite.
2. Add them to a newly instantiated deck instance.
3. Create new players.
4. Create a new CardGame instance.
5. Add the deck to the game.
6. Add the players to the game.
7. Shuffle.
8. Start game.
9. Play hand.